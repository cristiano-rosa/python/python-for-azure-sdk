# JUPYTER NOTEBOOKS

## Azure-Authentication.ipynb
The script checks which Python environment is being used. It authenticates to an Azure account using one of the available methods (Azure CLI, Azure PowerShell, etc.) and get the credentials. It retrieves the Azure subscription ID and then lists and prints the names of all resource groups in that Azure subscription. 