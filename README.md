# PYTHON FOR AZURE SDK

## 1) PREREQUISITES

### 1.1) Check / install your local Python environment for Azure development:    
Developing on Azure requires Python 3.7 or higher installed. To verify the version of Python on your workstation, in a console window type the command `python3 --version` for macOS/Linux or `py --version` for Windows.  
Python download: https://www.python.org/downloads/  

### 1.2) Create an Azure Account:    
To develop Python applications with Azure, you need an Azure account. Your Azure account is the credentials you use to sign-in to Azure with and what you use to create Azure resources.  
You can also create an Azure account for free: https://azure.microsoft.com/en-us/free/python/  

## 2) VIRTUAL ENVIRONMENT

### 2.1) Create Python Virtual Environment
```bash
cd <your_folder>
python -m venv .venv
```

### 2.2) Activate Python Virtual Environment
**macOS/Linux:**  
```bash
source .venv/bin/activate
```
**Windows:**  
```powershell
.venv\Scripts\activate.bat
```

## 3) OTHER REQUIREMENTS

### 3.1) Install requirements in the virtual environment
**Confirm you are in the virtual environment `(.venv)` before running `pip install`**  
```bash
pip install -r requirements.txt
```

### 3.2) Installing Jupyter
https://jupyter.org/install 

### 3.3) Create a Service Principal that can access resources

**3.3.1) Register an application with Azure AD and create a service principal (this is going to be your `AZURE_CLIENT_ID`):**  
https://learn.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal#register-an-application-with-azure-ad-and-create-a-service-principal  

**3.3.2) Assign a role to the application:**  
https://learn.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal#assign-a-role-to-the-application  

**3.3.3) Create a new application secret (this is going to be your `AZURE_CLIENT_SECRET`):**  
https://learn.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal#option-3-create-a-new-application-secret  

**3.3.4) After following the above steps, make sure you get from Azure the following values:**  
```powershell
AZURE_SUBSCRIPTION_ID = <your_azure_subscription_id>            <= You can get this from https://portal.azure.com/#view/Microsoft_Azure_Billing/SubscriptionsBlade
AZURE_TENANT_ID = <your_azure_tenant_id>                        <= You can get this from https://portal.azure.com/#view/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/~/Overview
AZURE_CLIENT_ID = <your_azure_application_client_id>            <= You have created this in the above step
AZURE_CLIENT_SECRET = <your_azure_application_client_secret>    <= You have created this in the above step
```

## 4) SETTINGS

### 4.1) Create `.env` file 
**The `.env` file will contain your Azure Subscripion ID, Azure Tenant ID, Azure Application Client ID and Azure Application Secret to connect to Azure.**    
**In the `.gitignore` file we are ignoring `*.env` so that your credentials don't get pushed to your repo.**  
**The file `<your_folder>/My-Scripts/settings.py` uses the `load_dotenv` function to load environment variables from the file named `.env` located in the same directory.**  
**So you have to create the file `<your_folder>/My-Scripts/.env` with the below content.**  
**Please get the proper values from Azure portal or CLI and change them accordingly.**  
```bash
# Load environment variables
AZURE_SUBSCRIPTION_ID = <your_azure_subscription_id>
AZURE_TENANT_ID = <your_azure_tenant_id>
AZURE_CLIENT_ID = <your_azure_application_client_id>
AZURE_CLIENT_SECRET = <your_azure_application_client_secret>

# Default variables
DEFAULT_RESOURCE_GROUP = 'default-resource-group'
DEFAULT_LOCATION = 'northeurope' 
```

### 4.2) Check settings from terminal
**In the below example, we import `AZURE_SUBSCRIPTION_ID` from settings, and if everything is configured correctly, you are going to see your Azure Subscription ID in the output.**  
```bash
cd My-Scripts
../.venv/bin/python Test-Settings.py
```

### 4.3) Steps to check settings from Jupyter Notebook

**4.3.1) Run the following commands from terminal:**  
```bash
source .venv/bin/activate
pip install ipykernel
python -m ipykernel install --user --name=.venv
jupyter notebook
```  

**4.3.2) Set Jupyter Notebook to use the proper Python virtual environment:**  
Once Jupyter Notebook is open, create a new notebook or open an existing one (e.g. My-Scripts/Test-Settings.ipynb).  
In the notebook, change the kernel to the one associated with your virtual environment:  
- Click on "Kernel" in the menu bar.  
- Select "Change kernel".  
- Select your virtual environment's kernel (it should be listed as `.venv`).  

**4.3.3) Then run the below two cells to check the settings:**  
Import the `AZURE_SUBSCRIPTION_ID` variable from the module named `settings` and then prints it out:  
It must show your Azure Subscription ID if your environment is properly configured.
```python
from settings import AZURE_SUBSCRIPTION_ID
print("Azure Subscription ID:", AZURE_SUBSCRIPTION_ID)
```
Use Python's built-in `sys` module to print the path of the Python executable that is running the script:  
It must show the path to your Python virtual environment `<your_folder>/.venv/bin/python`
```python
import sys
print(sys.executable)
```